class_name Train
extends Node2D

const CART := preload("res://cart.tscn")

const CART_LENGTH := 0.4

var is_running := false

@onready var level: Level = get_parent()

var _path: Array[Vector2i]
var _carts: Array[Cart]
var _cart_positions: Array[float]


func init(path: Array[Vector2i], carts: String):
	_path = path
	for cart in carts:
		_append_cart(cart)
	_append_cart("")
	z_index = 2


func _process(delta):
	if is_running:
		for i in len(_carts):
			_cart_positions[i] += delta * level.game_speed
			_update_cart(i)


func _append_cart(color: String):
	_carts.append(CART.instantiate())
	add_child(_carts[-1])
	_carts[-1].set_color(color)
	if _cart_positions == []:
		_cart_positions.append(CART_LENGTH / 2)
	else:
		_cart_positions.append(_cart_positions[-1] + CART_LENGTH)
	_update_cart(-1)
	return _carts[-1]


func _update_cart(i: int):
	_carts[i].position = _cart_position_to_local_position(_cart_positions[i])
	_carts[i].rotation = _cart_position_to_rotation(_cart_positions[i])


func _cart_position_to_local_position(cart_pos: float) -> Vector2:
	var global_pos = level.path_position_to_global_position(_path[cart_pos])
	var x_direction = _path[cart_pos + 1].x - _path[cart_pos].x
	var y_direction = _path[cart_pos + 1].y - _path[cart_pos].y
	var decimal_cart_pos = cart_pos - int(cart_pos)
	if y_direction == 0:
		global_pos.x += decimal_cart_pos * x_direction * Level.STEP.x * 2
	else:
		global_pos.y += decimal_cart_pos * y_direction * Level.STEP.y
		if x_direction == 0:
			global_pos.x += (
				decimal_cart_pos
				* (_path[cart_pos].y % 2 * 2 - 1)
				* Level.STEP.x)
		else:
			global_pos.x += decimal_cart_pos * x_direction * Level.STEP.x
	return to_local(global_pos)


func _cart_position_to_rotation(cart_pos: float) -> float:
	var x_direction = _path[cart_pos + 1].x - _path[cart_pos].x
	var y_direction = _path[cart_pos + 1].y - _path[cart_pos].y
	if x_direction == 0:
		return atan2(
			y_direction * Level.STEP.y,
			(_path[cart_pos].y % 2 * 2 - 1) * Level.STEP.x)
	else:
		return atan2(y_direction * Level.STEP.y, x_direction * Level.STEP.x)
