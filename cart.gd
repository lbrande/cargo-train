class_name Cart
extends Node2D

@onready var tile_map: TileMap = $TileMap


func set_color(color: String):
	match color:
		"O": tile_map.set_cell(0, Vector2i(0, 0), 0, Vector2i(0, 0))
		"P": tile_map.set_cell(0, Vector2i(0, 0), 0, Vector2i(0, 1))
		"R": tile_map.set_cell(0, Vector2i(0, 0), 0, Vector2i(0, 2))
		"Y": tile_map.set_cell(0, Vector2i(0, 0), 0, Vector2i(0, 3))
		"": tile_map.set_cell(0, Vector2i(0, 0), 0, Vector2i(0, 4))
