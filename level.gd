class_name Level
extends Node2D

const TILE := preload("res://tile.tscn")
const TRAIN := preload("res://train.tscn")

const WINDOW_SIZE := Vector2(1280, 720)
const LEVEL_SIZE := Vector2i(23, 8)

const STEP := Vector2(60, 106.5)
const ORIGIN := Vector2(
	(WINDOW_SIZE.x - (LEVEL_SIZE.x - 1) * STEP.x) / 2,
	(WINDOW_SIZE.y - (LEVEL_SIZE.y - 1) * STEP.y) / 2)

const DIAGONAL_ORIGIN := Vector2(ORIGIN.x, ORIGIN.y - STEP.y / 2)
const MAX_CLOSE_DISTANCE := 0.2
const MIN_FAR_DISTANCE := 0.3

@export var levels: Array[String]
var game_speed := 0.25

var _tiles: Array[Tile]
var _current_path: Array[Vector2i]
var _current_path_node: Line2D
var _track_graph: Dictionary
var _track_nodes: Dictionary


func _ready():
	_init_tiles()
	assert(len(levels) > 0, "No levels found")
	_load_level(levels[0])


func _process(_delta: float):
	_update_current_path()


func _input(event: InputEvent):
	if _is_left_click(event) and _current_path != []:
		if _current_path in _track_nodes:
			_remove_path(_current_path)
		else:
			_add_path(_current_path)


func _init_tiles():
	for y in range(LEVEL_SIZE.y):
		for x in range(LEVEL_SIZE.x):
			_tiles.append(TILE.instantiate())
			_tiles[-1].position = Vector2(x, y) * STEP + ORIGIN
			add_child(_tiles[-1])


func _load_level(level_path: String):
	var level_file = FileAccess.open(level_path, FileAccess.READ)
	var level = level_file.get_as_text().split("\n")
	level_file.close()
	assert(len(level) >= LEVEL_SIZE.y, "Invalid level height")
	for y in range(LEVEL_SIZE.y):
		assert(len(level[y]) >= LEVEL_SIZE.x, "Invalid level width")
		for x in range(LEVEL_SIZE.x):
			match level[y][x]:
				"`": _set_tile_as_terrain(x, y, 0)
				"-": _set_tile_as_terrain(x, y, 1)
				"^": _set_tile_as_terrain(x, y, 2)
				"#": _set_tile_as_terrain(x, y, 3)
				"O": _set_tile_as_city(x, y, 0)
				"P": _set_tile_as_city(x, y, 1)
				"R": _set_tile_as_city(x, y, 2)
				"Y": _set_tile_as_city(x, y, 3)


func _set_tile_as_terrain(x: int, y: int, type: int):
	_get_tile(x, y).tile_map.set_cell(0, Vector2i(0, 0), 0, Vector2i(type, 0))
	if (x + y) % 2 == 1:
		_get_tile(x, y).scale.y *= -1


func _set_tile_as_city(x: int, y: int, type: int):
	_get_tile(x, y).tile_map.set_cell(
		0, Vector2i(0, 0), 0, Vector2i(type, 1 + (x + y) % 2))


func _get_tile(x: int, y: int) -> Tile:
	return _tiles[y * LEVEL_SIZE.x + x]


func _update_current_path():
	var path = _path_at_mouse_position()
	if path != _current_path:
		if _current_path != []:
			_current_path = []
			remove_child(_current_path_node)
			_current_path_node = null
		if path != []:
			_current_path = path
			_current_path_node = _new_line2d_from_path(
				path, Color(1, 1, 1, 0.5))
			_current_path_node.z_index = 1
			add_child(_current_path_node)


func _path_at_mouse_position() -> Array[Vector2i]:
	var mouse_pos = get_local_mouse_position()
	var ldis = _diagonal_indices_of_path(_left_diagonal_index(mouse_pos))
	var rdis = _diagonal_indices_of_path(_right_diagonal_index(mouse_pos))
	if ldis != [] and rdis != [] and (ldis[0] != ldis[1] || rdis[0] != rdis[1]):
		return [
			Vector2i(_diagonal_indices_to_path_position(ldis[0], rdis[0])),
			Vector2i(_diagonal_indices_to_path_position(ldis[1], rdis[1]))]
	return []


func _left_diagonal_index(pos: Vector2) -> float:
	return (
		(pos.y - DIAGONAL_ORIGIN.y) / sqrt(3) +
		pos.x - DIAGONAL_ORIGIN.x) / 2 / STEP.x


func _right_diagonal_index(pos: Vector2) -> float:
	return (
		(DIAGONAL_ORIGIN.y - pos.y) / sqrt(3) +
		pos.x - DIAGONAL_ORIGIN.x) / 2 / STEP.x


func _diagonal_indices_of_path(di: float) -> Array[int]:
	if _distance_to_diagonal(di) < MAX_CLOSE_DISTANCE:
		return [round(di), round(di)]
	elif _distance_to_diagonal(di) > MIN_FAR_DISTANCE:
		return [floor(di), ceil(di)]
	return []


func _distance_to_diagonal(di: float) -> float:
	return 0.5 - abs(di - floor(di) - 0.5)


func _diagonal_indices_to_path_position(ldi: int, rdi: int) -> Vector2i:
	@warning_ignore("integer_division")
	return Vector2i(
		ceil(ldi / 2.0) + ceil(rdi / 2.0) - (ldi & rdi & 1), ldi - rdi)


func _is_left_click(event: InputEvent) -> bool:
	return (
		event is InputEventMouseButton
		and event.pressed
		and event.button_index == MOUSE_BUTTON_LEFT)


func _add_path(path: Array[Vector2i]):
	_track_nodes[path] = _new_line2d_from_path(path, Color.BLACK)
	add_child(_track_nodes[path])
	if path[0] not in _track_graph:
		_track_graph[path[0]] = [path[1]]
	else:
		_track_graph[path[0]].append(path[1])
	if path[1] not in _track_graph:
		_track_graph[path[1]] = [path[0]]
	else:
		_track_graph[path[1]].append(path[0])


func _remove_path(path: Array[Vector2i]):
	remove_child(_track_nodes[path])
	_track_nodes.erase(path)
	_track_graph[path[0]].erase(path[1])
	_track_graph[path[1]].erase(path[0])


func _new_line2d_from_path(path: Array[Vector2i], color: Color) -> Line2D:
	var line2d = Line2D.new()
	line2d.points = [
		path_position_to_global_position(path[0]),
		path_position_to_global_position(path[1])]
	line2d.default_color = color
	line2d.begin_cap_mode = Line2D.LINE_CAP_ROUND
	line2d.end_cap_mode = Line2D.LINE_CAP_ROUND
	return line2d


func path_position_to_global_position(path_pos: Vector2i) -> Vector2:
	return Vector2(
		(path_pos.x * 2 - path_pos.y % 2) * STEP.x + ORIGIN.x,
		(path_pos.y - 0.5) * STEP.y + ORIGIN.y)
